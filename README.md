# README #

ds


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
##### Installation: #####
The recommended way to install walklevel is by invoking the pip module at the command line.
First, upgrade pip to the latest version:
*python -m pip install --upgrade pip*
Then install the walklevel package with pip. We recommend a user install, using the *--user* flag to pip.
(note: don’t use sudo pip, that can generate problems) This installs the walklevel package for your local user, and does not
need extra permissions to write to the system directories:
*python -m pip install --user walklevel*
______________________________________________________________________________________________________________________________
___Note:__	For POSIX users (including Mac OS X and Linux users), these instructions assume the use of a [virtual environment](https://docs.python.org/3/library/venv.html).
For Windows users, these instructions assume that the option to adjust the system PATH environment variable was selected when
installing Python.
______________________________________________________________________________________________________________________________
Even though it's not recommended, it's also possible to install walklevel globally:
*python -m pip install walklevel*
________________________________________________________________________________________________________________________________
__Note:__ On POSIX systems you may need to enter the above command with *sudo* (i.e., *sudo -H python -m pip install walklevel*)
________________________________________________________________________________________________________________________________

##### Usage: #####
To use the _walklevel_ function included in the _walklevel_ package you first need to import it.
You can do it in two different ways:
*from walklevel import walkleve*
			OR
*from walklevel import \**
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact